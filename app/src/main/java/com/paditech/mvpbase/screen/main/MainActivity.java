package com.paditech.mvpbase.screen.main;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.paditech.mvpbase.R;
import com.paditech.mvpbase.common.mvp.activity.ActivityPresenter;
import com.paditech.mvpbase.common.mvp.activity.MVPActivity;
import com.paditech.mvpbase.screen.search.SearchFragment;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;

/**
 * Created by Nha Nha on 9/19/2017.
 */

public class MainActivity extends MVPActivity<MainContact.PresenterViewOps> implements MainContact.ViewOps {
    @BindView(R.id.tool_bar)
    Toolbar toolBar;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    private MainPagerAdapter mMainPagerAdapter;
    @Override
    protected int getContentView() {
        return R.layout.act_main;
    }

    @Override
    protected void initView() {
        setupViewPager();
    }

    @Override
    protected Class<? extends ActivityPresenter> onRegisterPresenter() {
        return MainPresenter.class;
    }

    private void setupViewPager() {
        mMainPagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setAdapter(mMainPagerAdapter);
        viewPager.setOffscreenPageLimit(4);
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            //hiển thị title và icon trong tab layout
            View view = LayoutInflater.from(this).inflate(R.layout.item_tab, null);
            TextView title = (TextView) view.findViewById(R.id.title);
            ImageView icon = (ImageView) view.findViewById(R.id.icon);
           /* title.setText(TITLES[i]);
            icon.setImageResource(ICONS[i]);*/
            tabLayout.getTabAt(i).setCustomView(view);
            final int index = i;
            tabLayout.getTabAt(i).getCustomView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switchTab(index);
                }
            });
        }
    }

    public void switchTab(int index) {
        switch (index) {
            case 0:
               // viewPager.setCurrentItem(index);
                break;
            case 1:
               /* if (PrefUtil.isLogin(this)) {
                    viewPager.setCurrentItem(index);
                } else {
                    replaceFragment(LoginFragment.newInstance(LoginFragment.LIST_QUOTATION), true);
                }*/
                break;
            case 2:
              /*  if (PrefUtil.isLogin(this)) {
                    viewPager.setCurrentItem(index);
                } else {
                    replaceFragment(LoginFragment.newInstance(LoginFragment.CONTRACT), true);
                }*/
                break;
            case 3:
               /* viewPager.setCurrentItem(index);
                EventBus.getDefault().post(new LoginEvent(0));*/
                break;
        }
    }

    private class MainPagerAdapter extends FragmentStatePagerAdapter {

        public MainPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            //trả về các màn con, mỗi màn là 1 tab, các màn hình này đều là fragment
            return new SearchFragment();
        }

        @Override
        public int getCount() {
            return 4;
        }
    }

}
