package com.paditech.mvpbase.screen.search;

import com.paditech.mvpbase.common.mvp.fragment.FragmentPresenterViewOps;
import com.paditech.mvpbase.common.mvp.fragment.FragmentViewOps;

/**
 * Created by Nha Nha on 1/2/2018.
 */

public interface SearchContact  {
    interface ViewOps extends FragmentViewOps{

    }
    interface PresenterViewOps extends FragmentPresenterViewOps{

    }
}
